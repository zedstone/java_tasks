package ua.petr;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class FireGame {
  //  Написать программу "стрельба по площади".
//
//  Технические требования:
//  Дан квадрат 5х5, где программа случайным образом ставит цель.
//  Перед началом игры на экран выводится текст: All set. Get ready to rumble!.
//  Сам процесс игры обрабатывается в бесконечном цикле.
//  Игроку предлагается ввести линию для стрельбы; программа проверяет что было введено число, и
//  введенная линия находится в границах игрового поля (1-5). В случае, если игрок ошибся, предлагает ввести число еще раз.
//  Игроку предлагается ввести столбик для стрельбы (должен проходить аналогичную проверку).
//  После каждого выстрела необходимо отображать обновленное игровое поле в консоли. Клетки,
//  куда игрок уже стрелял, необходимо отметить как *.
//  Игра заканчивается при поражении цели. В конце игры вывести в консоль фразу You have won!, а
//  также игровое поле. Пораженную цель отметить как x.
//  Задание должно быть выполнено используя массивы (НЕ используйте интерфейсы List, Set, Map).
  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);
    Random random = new Random();

    String[][] field = new String[5][5];
    for (String[] strings : field) {
      Arrays.fill(strings, "-");
    }
    int randomRow = random.nextInt(5);
    int randomCol = random.nextInt(5);

    field[randomRow][randomCol] = "O";

    System.out.println("All set. Get ready to rumble!");
    drawField(field);
    System.out.println();
    boolean endgame = false;

    do {
      int currentLine = -1;
      int currentColumn = -1;
      try {
        System.out.println("Enter the line of the shot (0-5)");
        currentLine = Integer.parseInt(scanner.nextLine());

        if (currentLine < 0 || currentLine > 5) {
          System.out.println("Input is not number 0-5");
          continue;
        }
      } catch (NumberFormatException e) {
        System.out.println("Input is not number 0-5");
        continue;
      }


      try {
        System.out.println("Enter the column of the shot (0-5)");
        currentColumn = Integer.parseInt(scanner.nextLine());

        if (currentColumn < 0 || currentColumn > 5) {
          System.out.println("Input is not number 0-5");
          continue;
        }
      } catch (NumberFormatException e) {
        System.out.println("Input is not number 0-5");
        continue;
      }

      if (currentLine - 1 == randomRow && currentColumn - 1 == randomCol) {
        field[randomRow][randomCol] = "X";
        System.out.println("You have won!");
        drawField(field);
        endgame = true;
      } else {
        field[currentLine - 1][currentColumn - 1] = "*";
        drawField(field);
      }

    } while (!endgame);

  }

  public static void drawField(String[][] field) {

    for (int i = 0; i < field.length + 1; i++) {
      System.out.print(i + " | ");
    }
    System.out.println();

    for (int i = 0; i < field.length; i++) {
      System.out.print(i + 1 + " |");
      for (int j = 0; j < field.length; j++) {
        System.out.printf(" %s |", field[i][j]);
      }
      System.out.println();
    }
  }


}

package ua.petr12.dao;

import ua.petr12.Family;

import java.util.List;

public interface FamilyDao {

  void loadData(List<Family> familyList);

  List<Family> getAllFamilies();

  Family getFamilyByIndex(int index);

  boolean deleteFamily(int index);

  boolean deleteFamily(Family family);

  void saveFamily(Family family);


}

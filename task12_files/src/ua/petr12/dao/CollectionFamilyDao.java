package ua.petr12.dao;

import ua.petr12.Family;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
  //private List<Family> families = new ArrayList<>();
  private List<Family> families;
  private static final String FILE_NAME = "task12_files/src/ua/petr12/data/data.txt";
  File file;

  public CollectionFamilyDao() {

    this.file = new File("task12_files/src/ua/petr12/data/data.txt");
    if(!file.exists()){
      try {
        file.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    this.families = getAllFamilies();
  }

  @Override
  public void loadData(List<Family> familyList){

    try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FILE_NAME))){
      oos.writeObject(familyList);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  public List<Family> readData(){
    try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILE_NAME))){
      return (List<Family>) ois.readObject();

    }catch(EOFException e) {
      //eof - no error in this case
      return new ArrayList<>();
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
      return new ArrayList<>();
    }

  }


  @Override
  public ArrayList<Family> getAllFamilies() {
    List<Family> dataFamilies = readData();

    return new ArrayList<>(dataFamilies);
    //return new ArrayList<>(families);

  }

  @Override
  public Family getFamilyByIndex(int index) {
    return families.get(index);
  }

  @Override
  public boolean deleteFamily(int index) {
    if(index > families.size() || index < 0){
      return false;
    }else {
      families.remove(index);
      loadData(families);
      return true;
    }
  }

  @Override
  public boolean deleteFamily(Family family) {
    if(!families.contains(family)){
      return false;
    }else {
      families.remove(family);
      loadData(families);
      return true;
    }
  }

  @Override
  public void saveFamily(Family family) {
    if(families.contains(family)){
     int index = families.indexOf(family);
     families.set(index, family);
     loadData(families);
    }else {
      families.add(family);
      loadData(families);

    }
  }
}

package ua.petr12.service;

import ua.petr12.Family;
import ua.petr12.Human;
import ua.petr12.Pet;

import java.util.List;
import java.util.Set;

public interface FamilyServiceInt {
  List<Family> getAllFamilies();

  Family getFamilyByIndex(int index);

  boolean deleteFamily(int index);

  boolean deleteFamily(Family family);

  void saveFamily(Family family);

  Family adoptChild(Family family, Human adoptChild);

  void loadData(List<Family> familyList);

  Family bornChild(Family family, String boyName, String girlName);

  int count();

  void createNewFamily(Human mother, Human father);

  void displayAllFamilies();

  void addPet(int index, Pet pet);

  Set<Pet> getPets(int index);

  void deleteAllChildrenOlderThen(int age);

  int countFamiliesWithMemberNumber(int num);

  List<Family> getFamiliesBiggerThan(int num);
  List<Family> getFamiliesLessThan(int num);
}

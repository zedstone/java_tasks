package ua.petr12.service;

import ua.petr12.Family;
import ua.petr12.Human;
import ua.petr12.Pet;
import ua.petr12.dao.CollectionFamilyDao;
import ua.petr12.dao.FamilyDao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;


public class FamilyService implements FamilyServiceInt {
  private FamilyDao familyDao = new CollectionFamilyDao();

  @Override
  public void loadData(List<Family> familyList){
    familyDao.loadData(familyList);
  }


  @Override
  public Family bornChild(Family family, String boyName, String girlName){
    Random random = new Random();
    if(random.nextInt(11) <= 5){
      System.out.println("Its a boy!");
      family.addChild(new Human(boyName, family.getFather().getSurname(), LocalDate.now()));
      return family;
    }else {
      family.addChild(new Human(girlName, family.getFather().getSurname(), LocalDate.now()));
      System.out.println("Its a girl!");
      return family;
    }

  }
  @Override
  public Family adoptChild(Family family, Human adoptChild){
    family.addChild(new Human(adoptChild.getName(), family.getFather().getSurname(), adoptChild.getYear()));
    saveFamily(family);
    return family;
  }

  @Override
  public int count(){
    return familyDao.getAllFamilies().size();
  }

  @Override
  public void createNewFamily(Human mother, Human father){
    familyDao.saveFamily(new Family(mother, father));
  }

  @Override
//  public void displayAllFamilies(){
//    System.out.println(new ArrayList<>(getAllFamilies()));
//  }

  public void displayAllFamilies(){
    getAllFamilies().forEach(System.out::println);
  }


  @Override
  public void addPet(int index, Pet pet) {
    Family currentFamily = familyDao.getFamilyByIndex(index);
    currentFamily.setPets(pet);
    familyDao.saveFamily(currentFamily);

  }

  @Override
  public Set<Pet> getPets(int index) {
    Family currentFamily = familyDao.getFamilyByIndex(index);
    return currentFamily.getPets();
  }

  @Override
//  public void deleteAllChildrenOlderThen(int age) {
//    for (int i = 0; i < familyDao.getAllFamilies().size(); i++) {
//      Family currentFamily = familyDao.getFamilyByIndex(i);
//      if(currentFamily.getChildren().size() > 0){
//        ArrayList<Human> arr =  currentFamily.getChildren();
//        for (int j = 0; j < currentFamily.getChildren().size(); j++) {
//          if(LocalDate.now().getYear() - (arr.get(i).getYear().getYear()) >= age){
//            arr.remove(i);
//          }
//          currentFamily.setChildren(arr);
//        }
//      }
//      familyDao.saveFamily(currentFamily);
//    }
//  }
  public void deleteAllChildrenOlderThen(int age) {
   List<Family> newFamilyList = familyDao.getAllFamilies().stream()
     .map(e -> {
       ArrayList<Human> childrenList = e.getChildren();
       e.setChildren(childrenList.stream().filter(c -> (LocalDate.now().getYear() - c.getYear().getYear()) <= age).collect(Collectors
         .toCollection(ArrayList::new)));
       return e;
     }).toList();
    for (Family family : newFamilyList) {  // думаю тут можно было по другому, буду рад позсказке
      familyDao.saveFamily(family);
    }
  }

  @Override
//  public int countFamiliesWithMemberNumber(int num) {
//    int counter = 0;
//    for (int i = 0; i < familyDao.getAllFamilies().size(); i++) {
//      if(familyDao.getAllFamilies().get(i).getFamilyLength() == num){
//        counter++;
//      }
//    }
//    return counter;
//  }
  public int countFamiliesWithMemberNumber(int num) {
    List<Family> newFamilyList = familyDao.getAllFamilies().stream()
      .filter(f -> f.getFamilyLength() == num).toList();
    return newFamilyList.size();
  }

  @Override
  public List<Family> getFamiliesBiggerThan(int num) {
    List<Family> newList = familyDao.getAllFamilies().stream().filter(f -> f.getFamilyLength() > num).toList();

    return newList;
  }

//  @Override
//  public List<Family> getFamiliesLessThan(int num) {
//    List<Family> newList = new ArrayList<>();
//    for (int i = 0; i < familyDao.getAllFamilies().size(); i++) {
//      if(familyDao.getAllFamilies().get(i).getFamilyLength() < num){
//        newList.add(familyDao.getFamilyByIndex(i));
//      }
//    }
//    return newList;
//  }

  @Override
  public List<Family> getFamiliesLessThan(int num) {
    List<Family> newList = familyDao.getAllFamilies().stream().filter(f -> f.getFamilyLength() < num).toList();

    return newList;
  }

  @Override
  public List<Family> getAllFamilies() {
    return familyDao.getAllFamilies();
  }

  @Override
  public Family getFamilyByIndex(int index) {

    return familyDao.getFamilyByIndex(index);
  }
  public void printFamily(int index) {
    familyDao.getFamilyByIndex(index).prettyFormat();
  }

  @Override
  public boolean deleteFamily(int index) {
    return familyDao.deleteFamily(index);
  }

  @Override
  public boolean deleteFamily(Family family) {
    return familyDao.deleteFamily(family);
  }

  @Override
  public void saveFamily(Family family) {
    familyDao.saveFamily(family);
  }
}

package ua.petr;

import java.util.Scanner;

public class planning {

//  Написать консольную программу "планирощик задач на неделю".
//
//  Технические требования:
//  Создать двумерный массив строк размерностью 7х2 String[][] schedule = new String[7][2]
//
//  Заполните матрицу значениями день недели: главное задание на данный день:
//  schedule[0][0] = "Sunday";
//  schedule[0][1] = "do home work";
//  schedule[1][0] = "Monday";
//  schedule[1][1] = "go to courses; watch a film";
//
//  Используя цикл и оператор switch, запросите у пользователя ввести день недели в консоль и, в зависимости от ввода:
//
//  программа: Please, input the day of the week:
//  пользователь вводит корректный день недели (f.e. Monday)
//  программа выводит на экран Your tasks for Monday: go to courses; watch a film.;
//  программа идет на следующую итерацию;
//  программа: Please, input the day of the week:
//  пользователь вводит некорректный день недели (f.e. %$=+!11=4)
//  программа выводит на экран Sorry, I don't understand you, please try again.;
//  программа идет на следующую итерацию до успешного ввода;
//  программа: Please, input the day of the week: пользователь выводит команду выхода exit
//  программа выходит из цикла и корректно завершает работу.
//  Задание должно быть выполнено ипспользуя массивы (НЕ используйте интерфейсы List, Set, Map).
//
//  Учтите: программа должна принимать команды как в нижнем регистре (monday) так и
//  в верхнем (MONDAY) и учитывать, что пользователь мог случайно после дня недели ввести пробел.

  public static void main(String[] args) {
    String[][] schedule = new String[7][2];

    schedule[0][0] = "Sunday";
    schedule[0][1] = "do home work";

    schedule[1][0] = "Monday";
    schedule[1][1] = "go to courses; watch a film";

    schedule[2][0] = "Tuesday";
    schedule[2][1] = "earn money";

    schedule[3][0] = "Wednesday";
    schedule[3][1] = "go on a trip";

    schedule[4][0] = "Thursday";
    schedule[4][1] = "go to the gym";

    schedule[5][0] = "Friday";
    schedule[5][1] = "kiss a girl";

    schedule[6][0] = "Saturday";
    schedule[6][1] = "write code, lots of code ";

    boolean exitSchedule = false;
    Scanner scanner = new Scanner(System.in);

    do{
      System.out.println("Please, input the day of the week (or exit):");
      String userEnterRaw = scanner.nextLine();

      String userEnter = userEnterRaw.toLowerCase().trim();
      switch (userEnter) {
        case "sunday":
          System.out.println(schedule[0][1]);
          break;
        case "monday":
          System.out.println(schedule[1][1]);
          break;
        case "tuesday":
          System.out.println(schedule[2][1]);
          break;
        case "wednesday":
          System.out.println(schedule[3][1]);
          break;
        case "thursday":
          System.out.println(schedule[4][1]);
          break;
        case "friday":
          System.out.println(schedule[5][1]);
          break;
        case "saturday":
          System.out.println(schedule[6][1]);
          break;
        case "exit":
          exitSchedule = true;
          break;
        default:
          System.out.println("Sorry, I don't understand you, please try again.");
          break;
      }
    }while (!exitSchedule);

  }
}

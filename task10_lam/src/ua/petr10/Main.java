package ua.petr10;

import ua.petr10.controller.FamilyController;

import java.time.LocalDate;

public class Main {
  public static void main(String[] args) {
    FamilyController familyController = new FamilyController();

    Human mother1 = new Human("Eva", "First", LocalDate.of(1986, 1, 22));
    Human father1 = new Human("Rich", "Bogdan", LocalDate.of(1988, 12, 27));
    Human mother2 = new Human("2Eva", "2First", LocalDate.of(1986, 1, 22));
    Human father2= new Human("2Rich", "2Bogdan", LocalDate.of(1988, 12, 27));


    Family bogdanFamily = new Family(mother1, father1);

    familyController.saveFamily(bogdanFamily);
    familyController.saveFamily(new Family(mother2, father2));


    familyController.displayAllFamilies();

    System.out.println("**********");

    Human newChild = new Human("John", "Dou", "04/12/1988",120);
    familyController.adoptChild(bogdanFamily,newChild);
    familyController.displayAllFamilies();
    System.out.println("**********");

    familyController.deleteAllChildrenOlderThen(40);
    familyController.displayAllFamilies();
    System.out.println(familyController.countFamiliesWithMemberNumber(2));

    System.out.println(familyController.getFamiliesLessThan(3));
    System.out.println(familyController.getFamiliesBiggerThan(2));
  }
}

package ua.petr10;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class Human {


  private String name;
  private String surname;
  private LocalDate year;
  private int iq;
  //private Days[][] schedule;
  private Map<Days,String> schedule= new HashMap<Days,String>();


  private Family family;

  public Human(String name, String surname, String year, int iq) {
    this.name = name;
    this.surname = surname;
    this.year = LocalDate.parse(year, DateTimeFormatter.ofPattern("d/MM/yyyy"));
    this.iq = iq;
  }

  public Human(String name, String surname, LocalDate year) {
    this.name = name;
    this.surname = surname;
    this.year = year;
  }

  public Human(String name, String surname, LocalDate year, int iq, Human mother, Human father, Map<Days,String> schedule) {
    this.name = name;
    this.surname = surname;
    this.year = year;
    this.iq = iq;
    this.schedule = schedule;
  }
  public Human() {

  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getYear() {
    return year;
  }

  public void setYear(LocalDate year) {
    this.year = year;
  }

  public int getIq() {
    return iq;
  }

  public void setIq(int iq) {
    this.iq = iq;
  }


  public Map<Days,String> getSchedule() {
    return schedule;
  }

  public void setSchedule(Map<Days,String> schedule) {
    this.schedule = schedule;
  }

  public String describeAge(){
    String result = "";
    LocalDate startDate = this.year;
    LocalDate endDate = LocalDate.now();
    Period period = Period.between(startDate, endDate);
    result = "Human lived : " + period.getYears() + " years " + period.getMonths() + " months " + period.getDays() + " days!";

    return result;
  }

  @Override
  public String toString() {
    return "Human{" +
      "name='" + name + '\'' +
      ", surname='" + surname + '\'' +
      ", year=" + year.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) +
      ", iq=" + iq +
      ", schedule=" + schedule +
      ", family=" + family +
      '}';
  }

  public void greetPet(){
    for (Pet pet :  this.family.getPets()) {
      System.out.printf("Привет, %s%n", pet.getNickname());
    }
  }
  public void describePet(){
    for (Pet pet :  this.family.getPets()) {
      int currentTrick= pet.getTrickLevel();
      System.out.printf("Привет, %s%n", pet.getNickname());
      if(currentTrick < 50){
        System.out.printf("У меня есть %s, ему %d лет, он почти не хитрый%n",pet.getSpecies(),pet.getAge());
      }else {
        System.out.printf("У меня есть %s, ему %d лет, он очень хитрый%n",pet.getSpecies(),pet.getAge());
      }
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Human human = (Human) o;
    return year == human.year && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(schedule, human.schedule) && Objects.equals(family, human.family);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, surname, year, iq, schedule, family);
  }

  @Override
  protected void finalize ( ) {
    System.err.println("сборщик мусора удаляет " + this);
  }
}

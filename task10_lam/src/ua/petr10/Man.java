package ua.petr10;

import java.time.LocalDate;
import java.util.Map;

public final class Man extends Human {
  public Man(String name, String surname, LocalDate year) {
    super(name, surname, year);
  }

  public Man(String name, String surname, LocalDate year, int iq, Pet pet, Human mother, Human father, Map<Days, String> schedule) {
    super(name, surname, year, iq, mother, father, schedule);
  }

  public Man() {
  }

  @Override
  public void greetPet() {
    {
      for (Pet pet :  getFamily().getPets()) {
        System.out.printf("О, опять ты, %s%n", pet.getNickname());
      }
    }
  }
  public void makeMoney(){
    System.out.println("Я сегодня заработал 27 000$");
  }
}

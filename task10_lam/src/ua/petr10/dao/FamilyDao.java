package ua.petr10.dao;

import ua.petr10.Family;

import java.util.List;

public interface FamilyDao {
  List<Family> getAllFamilies();

  Family getFamilyByIndex(int index);

  boolean deleteFamily(int index);

  boolean deleteFamily(Family family);

  void saveFamily(Family family);


}

package ua.petr7;
import java.util.*;

public class Main {
  public static void main(String[] args) {

    HashSet<String> dogHabits = new HashSet<>();
    dogHabits.add("sleep");
    dogHabits.add("eat");
    dogHabits.add("walk");
    Dog dog = new Dog("barsic",7,22, dogHabits);

    HashSet<String> catHabits = new HashSet<>();
    dogHabits.add("eat");
    dogHabits.add("scrappy");
    DomesticCat catMeow = new DomesticCat("MEOW_Ring",4,70, catHabits);

//    System.out.println(dog.toString());
//    dog.eat();
//    dog.respond();
//    dog.foul();

  //  System.out.println("==========");

    Human mother1 = new Human("Eva", "First", 1990);
    Human father1 = new Human("Rich", "Bogdan", 1991);

    HashMap<Days,String> child1Days = new HashMap<>();
    child1Days.put(Days.SATURDAY,"rest");
    child1Days.put(Days.SUNDAY,"rest");
    HashMap<Days,String> child2Days = new HashMap<>();
    child2Days.put(Days.WEDNESDAY,"work");

    Human child1 = new Human("Robert","Mariano", 2010,90,mother1,father1, child1Days);
    Human child2 = new Human("Elena","Mariano", 2014,95,mother1,father1, child2Days);


    Family bogdanFamily = new Family(mother1, father1);
    bogdanFamily.setPets(dog);
    bogdanFamily.setPets(catMeow);
    bogdanFamily.addChild(child1);
    bogdanFamily.addChild(child2);


    System.out.println(bogdanFamily.getChildren());
    System.out.println(bogdanFamily.getFamilyLength());
    bogdanFamily.deleteChild(child1);

    System.out.println(bogdanFamily.getFamilyLength());
    System.out.println("==========");



    Fish fish = new Fish();
    DomesticCat cat = new DomesticCat("murka");

    HashSet<String> robotHabits = new HashSet<>();
    robotHabits.add("calculate");
    robotHabits.add("calculate"); // check
    robotHabits.add("calculate3");
    RoboCat robot = new RoboCat("terminator", 300, 70, robotHabits);

    System.out.println(fish);
    System.out.println(cat);
    System.out.println(robot);

    Man man = new Man();
    man.makeMoney();

    Map<Days,String> map = new HashMap<>();
    Woman woman = new Woman("Ira", "rich" ,1991, 88, mother1,father1, map);
    System.out.println(woman);

    woman.setFamily(bogdanFamily);
    System.out.println("==========");

    woman.greetPet();
  }
}

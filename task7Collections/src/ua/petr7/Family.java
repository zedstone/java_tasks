package ua.petr7;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Family {
  private Human mother;
  private Human father;
//  private Human[] children = new Human[0];
  private ArrayList<Human> children= new ArrayList<>();
  //private Pet pet;
  private Set<Pet> pets = new HashSet<Pet>();
  private int familyLength;

  public int getFamilyLength() {
    return getChildren().size() + 2;
  }

  public void setFamilyLength(int familyLength) {
    this.familyLength = familyLength;
  }


  public Family(Human mother, Human father) {
    this.mother = mother;
    this.father = father;

  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

//  public Pet getPet() {
//    return pet;
//  }
//
//  public void setPet(Pet pet) {
//    this.pet = pet;
//  }

  public Set<Pet> getPets() {
    return pets;
  }

  public void setPets(Pet pet) {
    this.pets.add(pet);
  }

  public ArrayList<Human> getChildren() {
    return children;
  }

  public void setChildren(ArrayList<Human> children) {
    this.children = children;
  }

  @Override
  public String toString() {
    return "Family{" +
      "mother=" + mother +
      ", father=" + father +
      ", children=" + children +
      ", pets=" + pets +
      ", familyLength=" + familyLength +
      '}';
  }

  public void addChild(Human child) {
    getChildren().add(child);
  }

  public void deleteChild(Human child) {
    getChildren().remove(child);
  }
  @Override
  protected void finalize ( ) {
    System.err.println("сборщик мусора удаляет " + this);
  }

}

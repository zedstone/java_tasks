package ua.petr7;
import java.util.*;
public class Dog extends Pet {

  public void foul(){
    System.out.println("Нужно хорошо замести следы...");
  }

  @Override
  public void respond() {
    System.out.printf("Привет, хозяин. Я - %s. Я соскучился!%n", this.getNickname());
  }

  public Dog() {
    setSpecies(Pets.DOG);
  }

  public Dog(String nickname) {
    super(nickname);
    setSpecies(Pets.DOG);
  }

  public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
    super(nickname, age, trickLevel, habits);
    setSpecies(Pets.DOG);
  }
}

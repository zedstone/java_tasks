package ua.petr2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
  Human mother1;
  Human father1;
  Human child1;
  Human child2;
  Family testFamily;

  @org.junit.jupiter.api.BeforeEach

  void setUp() {

  }

  @org.junit.jupiter.api.AfterEach
  void tearDown() {
    mother1 = null;
    father1 = null;
    child1 = null;
    child2 = null;
    testFamily = null;
  }
  @Test
  public void addChildArrayLength(){
    Human mother1 = new Human("TestMother", "TestMotherSurName", 1990);
    Human father1 = new Human("TestFather", "TestFatherSurName", 1991);

    Human child1 = new Human("TestChild","TestChildSurName", 2010);
    Human child2 = new Human("TestChild2","TestChildSurName2", 2010);
    Family testFamily = new Family(mother1, father1);

    testFamily.addChild(child1);

    int startArrayLength = testFamily.getChildren().length;
    testFamily.addChild(child2);
    int afterAddArrayLength = testFamily.getChildren().length;

    assertTrue(startArrayLength - afterAddArrayLength == -1);

  }
  @Test
  public void addChildTestElement(){
    Human mother1 = new Human("TestMother", "TestMotherSurName", 1990);
    Human father1 = new Human("TestFather", "TestFatherSurName", 1991);

    Human child1 = new Human("TestChild","TestChildSurName", 2010);
    Human child2 = new Human("TestChild2","TestChildSurName2", 2010);
    Family testFamily = new Family(mother1, father1);

    boolean SuccessResult = true;
    testFamily.addChild(child1);

    if(!testFamily.getChildren()[testFamily.getChildren().length-1].equals(child1)){
      SuccessResult = false;
    }
    testFamily.addChild(child2);
    if(!testFamily.getChildren()[testFamily.getChildren().length-1].equals(child2)){
      SuccessResult = false;
    }
    assertTrue(SuccessResult);

  }
  @Test
  public void countFamilyCheck(){
    Human mother1 = new Human("TestMother", "TestMotherSurName", 1990);
    Human father1 = new Human("TestFather", "TestFatherSurName", 1991);

    Human child1 = new Human("TestChild","TestChildSurName", 2010);
    Human child2 = new Human("TestChild2","TestChildSurName2", 2010);
    Family testFamily = new Family(mother1, father1);

    boolean SuccessResult = true;
    if(testFamily.getChildren().length != 0){
      SuccessResult = false;
    }
    testFamily.addChild(child1);

    if(testFamily.getChildren().length != 1){
      SuccessResult = false;
    }
    testFamily.addChild(child2);
    if(testFamily.getChildren().length != 2){
      SuccessResult = false;
    }
    testFamily.deleteChild(child1);
    if(testFamily.getChildren().length != 1){
      SuccessResult = false;
    }
    assertTrue(SuccessResult);

  }
  @Test
  public void deleteChildNotValidArgument(){
    Human mother1 = new Human("TestMother", "TestMotherSurName", 1990);
    Human father1 = new Human("TestFather", "TestFatherSurName", 1991);

    Human child1 = new Human("TestChild","TestChildSurName", 2010);
    Human child2 = new Human("TestChild2","TestChildSurName2", 2010);
    Family testFamily = new Family(mother1, father1);


    testFamily.addChild(child1);

    Human[] childrenBefore = testFamily.getChildren();
   // System.out.println(Arrays.toString(childrenBefore));
    testFamily.deleteChild(child2);
    Human[] childrenAfter = testFamily.getChildren();
   // System.out.println(Arrays.toString(childrenAfter));

    assertArrayEquals(childrenBefore, childrenAfter);
  }

  @Test
  public void deleteChildCheck(){
    Human mother1 = new Human("TestMother", "TestMotherSurName", 1990);
    Human father1 = new Human("TestFather", "TestFatherSurName", 1991);

    Human child1 = new Human("TestChild","TestChildSurName", 2010);
    Family testFamily = new Family(mother1, father1);

    testFamily.addChild(child1);

    Human[] childrenBefore = testFamily.getChildren();
    // System.out.println(Arrays.toString(childrenBefore));
    testFamily.deleteChild(child1);
    Human[] childrenAfter = testFamily.getChildren();
    // System.out.println(Arrays.toString(childrenAfter));

    assertArrayEquals(childrenBefore, childrenAfter);
  }
}
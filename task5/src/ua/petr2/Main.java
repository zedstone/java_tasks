package ua.petr2;

import java.util.Arrays;

public class Main {
  public static void main(String[] args) {

    Pet dog = new Pet(Pets.DOG,"barsic",7,22, new String[] {"sleep", "eat", "walk"});

//    System.out.println(dog.toString());
//    dog.eat();
//    dog.respond();
//    dog.foul();

  //  System.out.println("==========");

    Human mother1 = new Human("Eva", "First", 1990);
    Human father1 = new Human("Rich", "Bogdan", 1991);

    Human child1 = new Human("Robert","Mariano", 2010,90,dog,mother1,father1,new Days[][]{ {Days.SUNDAY, Days.FRIDAY}, {Days.PARK, Days.POOL} });
    Human child2 = new Human("Elena","Mariano", 2014,95,dog,mother1,father1,new Days[][]{ {Days.SATURDAY, Days.THURSDAY}, {Days.BEACH} });

//    System.out.println(child1.toString());
//    child1.greetPet(child1.getPet());
//    child1.describePet(child1.getPet());
//
//    System.out.println("==========");

    Family bogdanFamily = new Family(mother1, father1);
    bogdanFamily.setPet(dog);
    bogdanFamily.addChild(child1);
    bogdanFamily.addChild(child2);


    System.out.println(Arrays.toString(bogdanFamily.getChildren()));
    System.out.println(bogdanFamily.getFamilyLength());
    bogdanFamily.deleteChild(child1);
    System.out.println(Arrays.toString(bogdanFamily.getChildren()));
    System.out.println(bogdanFamily.toString());
    System.out.println(bogdanFamily.getFamilyLength());
    System.out.println("==========");
    child1.setFamily(bogdanFamily);
    child1.greetPet();
    child1.describePet();
    System.out.println(child1.getFamily().getFather());
    child1.getFamily().getPet().respond();
    System.out.println("==========");
    System.out.println(child1.getFamily().getPet().toString());
    System.out.println(child1);

//    for (int i = 0; i < 200000; i++) {
//      Human human= new Human();
//      System.out.println(" " + i);
//    }
  }
}

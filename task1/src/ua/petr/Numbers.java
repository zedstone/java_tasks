package ua.petr;

import java.util.Random;
import java.util.Scanner;

public class Numbers {
  public static void main(String[] args) {
//    Технические требования:
//
//    С помощью java.util.Random программа загадывает случайное число в диапазоне [0-100]
//    и предлагает игроку через консоль ввести свое имя, которое сохраняется в переменной name.
//    Перед началом на экран выводится текст: Let the game begin!.
//    Сам процесс игры обрабатывается в бесконечном цикле.
//      Игроку предлагается ввести число в консоль, после чего программа сравнивает загаданное число с тем, что ввел пользователь.
//    Если введенное число меньше загаданного, то программа выводит на экран текст:
//    Your number is too small. Please, try again..
//    Если введенное число больше загаданного, то программа выводит на экран текст:
//    Your number is too big. Please, try again..
//    Если введенное число соответствуют загаданному, то программа выводит текст: Congratulations, {name}!.
//    Задание должно быть выполнено используя массивы (НЕ используйте интерфейсы List, Set, Map).

    System.out.println("Let the game begin!");

    Random random = new Random();
    int goal = random.nextInt(101);

    Scanner scanner = new Scanner(System.in);
    System.out.println("Enter your name");

    String userName = scanner.nextLine();
    int userNumber = -1;
    do {
      System.out.println("Enter your number please [0-100]");
      try {
        userNumber = Integer.parseInt(scanner.nextLine());

        if(userNumber<goal) {
          System.out.println("Your number is too small");
        }
        if(userNumber>goal){
          System.out.println("Your number is too big");
        }

      } catch (NumberFormatException e) {
        System.out.println("Input String not a number.");
      }

    }while (userNumber != goal);
    System.out.printf("Congratulations %s", userName);
  }
}

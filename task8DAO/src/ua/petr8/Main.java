package ua.petr8;

import ua.petr8.controller.FamilyController;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Main {
  public static void main(String[] args) {
    FamilyController familyController = new FamilyController();

    Human mother1 = new Human("Eva", "First", 1990);
    Human father1 = new Human("Rich", "Bogdan", 1991);

    Human mother2 = new Human("mother2", "sss", 1987);
    Human father2 = new Human("father2", "rrr", 1988);


    Family bogdanFamily = new Family(mother1, father1);

    familyController.saveFamily(bogdanFamily);
    familyController.saveFamily(new Family(mother2, father2));

    System.out.println(familyController.getAllFamilies());
    familyController.displayAllFamilies();

    System.out.println("$$$$$$$$$");

    System.out.println(familyController.getFamilyByIndex(1));

    familyController.deleteFamily(bogdanFamily);
    familyController.createNewFamily(new Human("Kate", "Cool", 1987), new Human("Boris", "Store", 1987));
    System.out.println(familyController.getAllFamilies());

    System.out.println("$$$$$$$$$");
    System.out.print("How many families in DB: ");
    System.out.println(familyController.count());

    System.out.println("$$$$$$$$$");
    familyController.saveFamily(bogdanFamily);
    Human newAdoptChild = new Human("Vadim", "Zelen", 12);
    familyController.bornChild(bogdanFamily, "coolBOY", "superGirl");
    familyController.adoptChild(bogdanFamily, newAdoptChild);
    familyController.displayAllFamilies();

    System.out.println("$$$$$$$$$");
    familyController.addPet(0, new Fish("Neon"));
    familyController.addPet(0, new Dog("Bigly"));

    familyController.displayAllFamilies();
    System.out.println(familyController.getPets(0));
    System.out.println("$$$$$$$$$");
    familyController.deleteFamily(0);
    familyController.adoptChild(familyController.getFamilyByIndex(0), new Human("Vadim2", "Zelen", 19));
    familyController.adoptChild(bogdanFamily, new Human("Vadim3", "Zelen", 21));
    familyController.displayAllFamilies();

    familyController.deleteAllChildrenOlderThen(18);
    familyController.displayAllFamilies();
    System.out.println("$$$$$$$$$");
    familyController.createNewFamily(new Human("test", "test", 33), new Human("test", "test" ,33));
    System.out.println(familyController.countFamiliesWithMemberNumber(1));
    System.out.println(familyController.countFamiliesWithMemberNumber(2));
    System.out.println(familyController.countFamiliesWithMemberNumber(5));

    System.out.println("$$$$$$$$$");
    System.out.println(familyController.getFamiliesBiggerThan(4));
    System.out.println(familyController.getFamiliesLessThan(3));

  }
}

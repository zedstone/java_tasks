package ua.petr8;

public enum Days {
      MONDAY,
      TUESDAY,
      WEDNESDAY,
      THURSDAY,
      FRIDAY,
      SATURDAY,
      SUNDAY,
      REST,
      WORK,
      POOL,
      DATE,
      BEACH,
      PARK;
}

package ua.petr8;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public abstract class Pet {

  private Pets species;
  private String nickname;
  private int age;
  private int trickLevel;
  //private String[] habits;
  Set<String> habits = new HashSet<>();

  public void eat(){
    System.out.println("Я кушаю!");
  }
  public abstract void respond();
  // System.out.printf("Привет, хозяин. Я - %s. Я соскучился!%n", this.getNickname());
  public Pet() {
    this.species = Pets.UNKNOWN;
  }

  public Pet(String nickname) {
    this.species = Pets.UNKNOWN;
    this.nickname = nickname;
  }

  public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
    this.species = Pets.UNKNOWN;
    this.nickname = nickname;
    this.age = age;
    this.trickLevel = trickLevel;
    this.habits = habits;
  }

  public Pets getSpecies() {
    return species;
  }

  public void setSpecies(Pets species) {
    this.species = species;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public int getTrickLevel() {
    return trickLevel;
  }

  public void setTrickLevel(int trickLevel) {
    this.trickLevel = trickLevel;
  }

//  public String[] getHabits() {
//    return habits;
//  }
//
//  public void setHabits(String[] habits) {
//    this.habits = habits;
//  }

  public Set<String> getHabits() {
    return habits;
  }

  public void setHabits(Set<String> habits) {
    this.habits = habits;
  }

  @Override
  public String toString() {
    return "Pet{" +
      "species=" + species +
      ", nickname='" + nickname + '\'' +
      ", age=" + age +
      ", trickLevel=" + trickLevel +
      ", habits=" + habits +
      '}';
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Pet pet = (Pet) o;
    return age == pet.age && trickLevel == pet.trickLevel && species == pet.species && Objects.equals(nickname, pet.nickname) && Objects.equals(habits, pet.habits);
  }

  @Override
  public int hashCode() {
    return Objects.hash(species, nickname, age, trickLevel, habits);
  }

  @Override
  protected void finalize ( ) {
    System.err.println("сборщик мусора удаляет " + this);
  }
}

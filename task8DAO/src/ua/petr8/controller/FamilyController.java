package ua.petr8.controller;

import ua.petr8.Family;
import ua.petr8.Human;
import ua.petr8.Pet;
import ua.petr8.service.FamilyService;
import ua.petr8.service.FamilyServiceInt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class FamilyController {
  private FamilyServiceInt familyService = new FamilyService();

  public List<Family> getFamiliesBiggerThan(int num) {
    return familyService.getFamiliesBiggerThan(num);
  }
  public List<Family> getFamiliesLessThan(int num) {
    return familyService.getFamiliesLessThan(num);
  }

  public int countFamiliesWithMemberNumber(int num) {
    return familyService.countFamiliesWithMemberNumber(num);
  }

  public void deleteAllChildrenOlderThen(int age) {
    familyService.deleteAllChildrenOlderThen(age);
  }

  public Set<Pet> getPets(int index) {
    return familyService.getPets(index);
  }

  public void addPet(int index, Pet pet) {
    familyService.addPet(index, pet);
  }

  public Family adoptChild(Family family, Human adoptChild){
    return familyService.adoptChild(family, adoptChild);
  }

  public Family bornChild(Family family, String boyName, String girlName){
    Random random = new Random();
    if(random.nextInt(11) <= 5){
      System.out.println("Its a boy!");
      family.addChild(new Human(boyName, family.getFather().getSurname(), 0));
      saveFamily(family);

      return family;
    }else {
      family.addChild(new Human(girlName, family.getFather().getSurname(), 0));
      System.out.println("Its a girl!");
      saveFamily(family);

      return family;
    }

  }

  public int count(){
    return familyService.getAllFamilies().size();
  }

  public void createNewFamily(Human mother, Human father){
    familyService.saveFamily(new Family(mother, father));
  }

  public void displayAllFamilies(){
    System.out.println(new ArrayList<>(familyService.getAllFamilies()));
  }

  public List<Family> getAllFamilies() {
    return familyService.getAllFamilies();
  }


  public Family getFamilyByIndex(int index) {
    return familyService.getFamilyByIndex(index);
  }


  public boolean deleteFamily(int index) {
    return familyService.deleteFamily(index);
  }


  public boolean deleteFamily(Family family) {
    return familyService.deleteFamily(family);
  }

  public void saveFamily(Family family) {
    familyService.saveFamily(family);
  }

}

package ua.petr9;

import ua.petr9.controller.FamilyController;

import java.time.LocalDate;

public class Main {
  public static void main(String[] args) {
    FamilyController familyController = new FamilyController();

    Human mother1 = new Human("Eva", "First", LocalDate.of(1986, 1, 22));
    Human father1 = new Human("Rich", "Bogdan", LocalDate.of(1988, 12, 27));



    Family bogdanFamily = new Family(mother1, father1);

    familyController.saveFamily(bogdanFamily);

    familyController.displayAllFamilies();
    System.out.println(mother1.describeAge());
    System.out.println("**********");

    Human newChild = new Human("John", "Dou", "22/08/1998",120);
    System.out.println(newChild.describeAge());
    familyController.adoptChild(bogdanFamily,newChild);
    familyController.displayAllFamilies();
  }
}

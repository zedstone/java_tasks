package ua.petr9.dao;

import ua.petr9.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
  private List<Family> families = new ArrayList<>();

  @Override
  public List<Family> getAllFamilies() {
    return new ArrayList<>(families);
  }

  @Override
  public Family getFamilyByIndex(int index) {
    return families.get(index);
  }

  @Override
  public boolean deleteFamily(int index) {
    if(index > families.size() || index < 0){
      return false;
    }else {
      families.remove(index);
      return true;
    }
  }

  @Override
  public boolean deleteFamily(Family family) {
    if(!families.contains(family)){
      return false;
    }else {
      families.remove(family);
      return true;
    }
  }

  @Override
  public void saveFamily(Family family) {
    if(families.contains(family)){
     int index = families.indexOf(family);
     families.set(index, family);
    }else {
      families.add(family);

    }
  }
}

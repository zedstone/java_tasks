package ua.petr9.service;

import ua.petr9.Family;
import ua.petr9.Human;
import ua.petr9.Pet;
import ua.petr9.dao.CollectionFamilyDao;
import ua.petr9.dao.FamilyDao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;



public class FamilyService implements FamilyServiceInt {
  private FamilyDao familyDao = new CollectionFamilyDao();


  @Override
  public Family bornChild(Family family, String boyName, String girlName){
    Random random = new Random();
    if(random.nextInt(11) <= 5){
      System.out.println("Its a boy!");
      family.addChild(new Human(boyName, family.getFather().getSurname(), LocalDate.now()));
      return family;
    }else {
      family.addChild(new Human(girlName, family.getFather().getSurname(), LocalDate.now()));
      System.out.println("Its a girl!");
      return family;
    }

  }
  @Override
  public Family adoptChild(Family family, Human adoptChild){
    family.addChild(new Human(adoptChild.getName(), family.getFather().getSurname(), adoptChild.getYear()));
    saveFamily(family);
    return family;
  }

  @Override
  public int count(){
    return familyDao.getAllFamilies().size();
  }

  @Override
  public void createNewFamily(Human mother, Human father){
    familyDao.saveFamily(new Family(mother, father));
  }

  @Override
  public void displayAllFamilies(){
    System.out.println(new ArrayList<>(getAllFamilies()));
  }

  @Override
  public void addPet(int index, Pet pet) {
    Family currentFamily = familyDao.getFamilyByIndex(index);
    currentFamily.setPets(pet);
    familyDao.saveFamily(currentFamily);

  }

  @Override
  public Set<Pet> getPets(int index) {
    Family currentFamily = familyDao.getFamilyByIndex(index);
    return currentFamily.getPets();
  }

  @Override
  public void deleteAllChildrenOlderThen(int age) {
    for (int i = 0; i < familyDao.getAllFamilies().size(); i++) {
      Family currentFamily = familyDao.getFamilyByIndex(i);
      if(currentFamily.getChildren().size() > 0){
        ArrayList<Human> arr =  currentFamily.getChildren();
        for (int j = 0; j < currentFamily.getChildren().size(); j++) {
          if(LocalDate.now().getYear() - (arr.get(i).getYear().getYear()) >= age){
            arr.remove(i);
          }
          currentFamily.setChildren(arr);
        }
      }
      familyDao.saveFamily(currentFamily);
    }
  }

  @Override
  public int countFamiliesWithMemberNumber(int num) {
    int counter = 0;
    for (int i = 0; i < familyDao.getAllFamilies().size(); i++) {
      if(familyDao.getAllFamilies().get(i).getFamilyLength() == num){
        counter++;
      }
    }
    return counter;
  }

  @Override
  public List<Family> getFamiliesBiggerThan(int num) {
    List<Family> newList = new ArrayList<>();
    for (int i = 0; i < familyDao.getAllFamilies().size(); i++) {
      if(familyDao.getAllFamilies().get(i).getFamilyLength() > num){
        newList.add(familyDao.getFamilyByIndex(i));
      }
    }
    return newList;
  }

  @Override
  public List<Family> getFamiliesLessThan(int num) {
    List<Family> newList = new ArrayList<>();
    for (int i = 0; i < familyDao.getAllFamilies().size(); i++) {
      if(familyDao.getAllFamilies().get(i).getFamilyLength() < num){
        newList.add(familyDao.getFamilyByIndex(i));
      }
    }
    return newList;
  }

  @Override
  public List<Family> getAllFamilies() {
    return familyDao.getAllFamilies();
  }

  @Override
  public Family getFamilyByIndex(int index) {
    return familyDao.getFamilyByIndex(index);
  }

  @Override
  public boolean deleteFamily(int index) {
    return familyDao.deleteFamily(index);
  }

  @Override
  public boolean deleteFamily(Family family) {
    return familyDao.deleteFamily(family);
  }

  @Override
  public void saveFamily(Family family) {
    familyDao.saveFamily(family);
  }
}

package ua.petr9;

import java.util.Set;

public class RoboCat extends Pet {
  public void foul(){
    System.out.println("Роботы никогда не делают пакости....почти");
  }
  @Override
  public void respond() {
    System.out.println("100010010010 110001100100 0001 00011010111 0101100");
  }

  public RoboCat() {
    setSpecies(Pets.ROBOCAT);
  }

  public RoboCat(String nickname) {
    super(nickname);
    setSpecies(Pets.ROBOCAT);
  }

  public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
    super(nickname, age, trickLevel, habits);
    setSpecies(Pets.ROBOCAT);
  }
}

package ua.petr9;

import java.time.LocalDate;
import java.util.Map;

public final class Woman extends Human {
  public Woman(String name, String surname, LocalDate year) {
    super(name, surname, year);
  }

  public Woman(String name, String surname, LocalDate year, int iq, Human mother, Human father, Map<Days, String> schedule) {
    super(name, surname, year, iq, mother, father, schedule);
  }

  public Woman() {
  }

  @Override
  public void greetPet() {
    {
      for (Pet pet :  getFamily().getPets()) {
        System.out.printf("%s, ути пути пути %n", pet.getNickname());
      }

    }
  }
  public void makeParty(){
    System.out.println("Я сегодня приготовила торт!");
  }
}

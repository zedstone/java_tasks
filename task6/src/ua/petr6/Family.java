package ua.petr6;

import java.util.Arrays;

public class Family {
  private Human mother;
  private Human father;
  private Human[] children = new Human[0];
  private Pet pet;
  private int familyLength;

  public int getFamilyLength() {
    familyLength = children.length + 2;
    return familyLength;
  }

  public void setFamilyLength(int familyLength) {
    this.familyLength = familyLength;
  }


  public Family(Human mother, Human father) {
    this.mother = mother;
    this.father = father;

  }

  public Human getMother() {
    return mother;
  }

  public void setMother(Human mother) {
    this.mother = mother;
  }

  public Human getFather() {
    return father;
  }

  public void setFather(Human father) {
    this.father = father;
  }

  public Human[] getChildren() {
    return children;
  }

  public void setChildren(Human[] children) {
    this.children = children;
  }

  public Pet getPet() {
    return pet;
  }

  public void setPet(Pet pet) {
    this.pet = pet;
  }

  @Override
  public String toString() {

    return "Family{" +
      "mother=" + mother.getName() + " " + mother.getSurname() +
      ", father=" + father.getName() + " " + father.getSurname() +
      ", children=" + Arrays.toString(children) +
      ", pet=" + pet.getNickname() +
      '}';
  }

  public void addChild(Human child) {
    children = Arrays.copyOf(children, children.length + 1);
    children[children.length - 1] = child;

  }

  public void deleteChild(Human child) {

    Human[] newArr = null;

    for (int i = 0; i < children.length - 1; i++) {
      if (children[i] == child) {
        newArr = new Human[children.length - 1];
        for (int index = 0; index < i; index++) {
          newArr[index] = children[index];
        }
        for (int j = i; j < children.length - 1; j++) {
          newArr[j] = children[j + 1];
        }
        children = Arrays.copyOf(newArr, children.length - 1);
        break;
      }
    }
  }
  @Override
  protected void finalize ( ) {
    System.err.println("сборщик мусора удаляет " + this);
  }

}

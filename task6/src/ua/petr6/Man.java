package ua.petr6;

public final class Man extends Human{
  public Man(String name, String surname, int year) {
    super(name, surname, year);
  }

  public Man(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, Days[][] schedule) {
    super(name, surname, year, iq, mother, father, schedule);
  }

  public Man() {
  }

  @Override
  public void greetPet() {
    {
     System.out.printf("О, опять ты, %s%n", this.getFamily().getPet().getNickname());

    }
  }
  public void makeMoney(){
    System.out.println("Я сегодня заработал 27 000$");
  }
}

package ua.petr6;

public class DomesticCat extends Pet{

  public void foul(){
    System.out.println("Ой...кажеться упала елка");
  }
  @Override
  public void respond() {
    System.out.println("Мяуууу мяууу, рада тебя видеть!");
  }

  public DomesticCat() {
    setSpecies(Pets.DOMESTICCAT);
  }

  public DomesticCat(String nickname) {
    super(nickname);
    setSpecies(Pets.DOMESTICCAT);
  }

  public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
    super(nickname, age, trickLevel, habits);
    setSpecies(Pets.DOMESTICCAT);
  }
}

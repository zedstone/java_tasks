package ua.petr6;

public final class Woman extends Human{
  public Woman(String name, String surname, int year) {
    super(name, surname, year);
  }

  public Woman(String name, String surname, int year, int iq, Human mother, Human father, Days[][] schedule) {
    super(name, surname, year, iq, mother, father, schedule);
  }

  public Woman() {
  }

  @Override
  public void greetPet() {
    {
      System.out.printf("%s, ути пути пути %n", getFamily().getPet().getNickname());
    }
  }
  public void makeParty(){
    System.out.println("Я сегодня приготовила торт!");
  }
}

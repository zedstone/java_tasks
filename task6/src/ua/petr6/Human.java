package ua.petr6;

import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

public class Human {


  private String name;
  private String surname;
  private int year;
  private int iq;
 // private Pet pet;
 // private Human mother;
 // private Human father;
  private Days[][] schedule;


  private Family family;

  public Human(String name, String surname, int year) {
    this.name = name;
    this.surname = surname;
    this.year = year;
  }

//  public Human(String name, String surname, int year, Human mother, Human father) {
//    this.name = name;
//    this.surname = surname;
//    this.year = year;
//    this.mother = mother;
//    this.father = father;
//  }

  public Human(String name, String surname, int year, int iq, Human mother, Human father, Days[][] schedule) {
    this.name = name;
    this.surname = surname;
    this.year = year;
    this.iq = iq;
//    this.pet = pet;
//    this.mother = mother;
//    this.father = father;
    this.schedule = schedule;
  }
  public Human() {

  }

  public Family getFamily() {
    return family;
  }

  public void setFamily(Family family) {
    this.family = family;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public int getIq() {
    return iq;
  }

  public void setIq(int iq) {
    this.iq = iq;
  }

//  public Pet getPet() {
//    return pet;
//  }

//  public void setPet(Pet pet) {
//    this.pet = pet;
//  }
//
//  public Human getMother() {
//    return mother;
//  }
//
//  public void setMother(Human mother) {
//    this.mother = mother;
//  }
//
//  public Human getFather() {
//    return father;
//  }
//
//  public void setFather(Human father) {
//    this.father = father;
//  }

  public Days[][] getSchedule() {
    return schedule;
  }

  public void setSchedule(Days[][] schedule) {
    this.schedule = schedule;
  }

  @Override
  public String toString() {
    return "Human{" +
      "name='" + name + '\'' +
      ", surname='" + surname + '\'' +
      ", year=" + year +
      ", iq=" + iq +
      ", schedule=" + Arrays.deepToString(schedule).toLowerCase(Locale.ROOT) +
      '}';
  }

  public void greetPet(){
    System.out.printf("Привет, %s%n", this.family.getPet().getNickname());
  }
  public void describePet(){
    int currentTrick= this.family.getPet().getTrickLevel();
    if(currentTrick < 50){
      System.out.printf("У меня есть %s, ему %d лет, он почти не хитрый%n",this.family.getPet().getSpecies(),this.family.getPet().getAge());
    }else {
      System.out.printf("У меня есть %s, ему %d лет, он очень хитрый%n",this.family.getPet().getSpecies(),this.family.getPet().getAge());
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Human human = (Human) o;
    return year == human.year && iq == human.iq && name.equals(human.name) && surname.equals(human.surname) && Arrays.equals(schedule, human.schedule);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(name, surname, year, iq);
    result = 31 * result + Arrays.deepHashCode(schedule);
    return result;
  }

  @Override
  protected void finalize ( ) {
    System.err.println("сборщик мусора удаляет " + this);
  }
}

package ua.petr6;

public class Fish extends Pet{
  @Override
  public void respond() {
    System.out.println("буль...буль...буль...");
  }

  public Fish() {
    setSpecies(Pets.FISH);
  }

  public Fish(String nickname) {
    super(nickname);
    setSpecies(Pets.FISH);
  }

  public Fish(String nickname, int age, int trickLevel, String[] habits) {
    super(nickname, age, trickLevel, habits);
    setSpecies(Pets.FISH);
  }
}

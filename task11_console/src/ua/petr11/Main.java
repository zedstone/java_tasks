package ua.petr11;

import ua.petr11.controller.FamilyController;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Main {
  public static void main(String[] args) {
    FamilyController familyController = new FamilyController();

//    familyController.printFamily(0);
//    familyController.printFamily(1);

    Scanner scanner = new Scanner(System.in);
    while (true){

      System.out.println("ГЛАВНОЕ МЕНЮ");
      System.out.println("==============================");
      System.out.println("1. Заполнить тестовыми данными");
      System.out.println("2. Отобразить весь список семей");
      System.out.println("3. Отобразить список семей, где количество людей больше заданного");
      System.out.println("4. Отобразить список семей, где количество людей меньше заданного");
      System.out.println("5. Подсчитать количество семей, где количество членов равно");
      System.out.println("6. Создать новую семью");
      System.out.println("7. Удалить семью по индексу");
      System.out.println("8. Редактировать семью по индексу семьи в общем списке");
      System.out.println("9. Удалить всех детей старше указанного возраста возраста");
      System.out.println("10. Просмотреть семью по номеру");
      System.out.println("11. Выход");

      System.out.println("");
      System.out.println("Выберите пункт меню");

      int userInput = 0;
      try {
        userInput = scanner.nextInt();
        scanner.nextLine();


      }catch (RuntimeException e){
        System.err.println("Вы ввели не корректное число");
        scanner.next();
        userInput = 0;
      }


      if(userInput == 1){


        Human mother1 = new Human("Eva", "First", LocalDate.of(1990, 1, 22));
        Human father1 = new Human("Rich", "Bogdan", LocalDate.of(1991, 12, 27));

        Human mother2 = new Human("Svetlana", "Second", LocalDate.of(1986, 1, 12));
        Human father2= new Human("Vadim", "Petrov", LocalDate.of(1988, 12, 27));

        Human mother3 = new Human("Natasha", "Klov", LocalDate.of(1977, 1, 4));
        Human father3= new Human("Andrey", "Prot", LocalDate.of(1980, 12, 14));

        Family bogdanFamily = new Family(mother1, father1);
        Family petrovFamily = new Family(mother2, father2);
        Family protFamily = new Family(mother3, father3);

        familyController.saveFamily(bogdanFamily);
        familyController.saveFamily(petrovFamily);
        familyController.saveFamily(protFamily);

        bogdanFamily.setPets(new Dog("Barsic", 5, 30, new HashSet<String>(Set.of("eat", "play"))));
        bogdanFamily.setPets(new Fish("Boo", 5, 30, new HashSet<String>(Set.of("eat", "sleep"))));


        Human newChild = new Human("John", "Dou", "04/12/1988",150);
        Human newChild2 = new Human("Maria", "Dou", "05/08/2008",120);
        Human newChild3 = new Human("Edik", "Dou", "12/06/2005",120);


        familyController.adoptChild(bogdanFamily,newChild);
        familyController.adoptChild(bogdanFamily,newChild2);
        familyController.adoptChild(petrovFamily,newChild3);

        System.out.println("Тестовые данные записаны");
      }

      //
      if (userInput == 2){
        familyController.displayAllFamilies();
      }

      //
      if (userInput == 3){

        while (true){
          System.out.println("Введите число что бы отобразить список семей, где количество людей больше заданного");
          try {
            int userNum = scanner.nextInt();
            System.out.println(familyController.getFamiliesBiggerThan(userNum));;
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
      }

      //
      if (userInput == 4){
        while (true){
          System.out.println("Введите число что бы отобразить список семей, где количество людей меньше заданного");
          try {
            int userNum = scanner.nextInt();
            System.out.println(familyController.getFamiliesLessThan(userNum));;
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
      }
      //
      if (userInput == 5){
        while (true){
          System.out.println("Введите число что бы подсчитать количество семей, где количество членов равно ему");
          try {
            int userNum = scanner.nextInt();
            System.out.println(familyController.countFamiliesWithMemberNumber(userNum));;
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
      }
      //
      if (userInput == 6){
        int motherYearBorn = 0;
        int motherMonthBorn=0;
        int motherDayBorn=0;
        int motherIq = 0;
        String motherName ="";
        String motherSurName ="";

          try {
            System.out.println("Введите имя Матери");
            motherName = scanner.nextLine().trim();
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
          try {
            System.out.println("Введите фамилию Матери");
            motherSurName = scanner.nextLine().trim();
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }



          while (true){
            System.out.println("Введите год рождения Матери");
            try {
              motherYearBorn = scanner.nextInt();
              if (motherYearBorn<1900 || motherYearBorn> 2050){
                continue;
              }
              break;
            }catch (RuntimeException e){
              System.err.println("Вы ввели не корректное число");
              scanner.next();
            }
          }
        while (true){
          System.out.println("Введите месяц рождения Матери");
          try {
            motherMonthBorn = scanner.nextInt();
            if (motherMonthBorn<0 || motherMonthBorn> 12){
              continue;
            }
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
        while (true){
          System.out.println("Введите день рождения Матери");
          try {
            motherDayBorn = scanner.nextInt();
            if (motherDayBorn<0 || motherDayBorn> 31){
              continue;
            }
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
        while (true){
          System.out.println("Введите IQ Матери");
          try {
            motherIq = scanner.nextInt();
            if (motherIq<0 || motherIq> 220){
              continue;
            }
            scanner.nextLine();
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }

        int fatherYearBorn = 0;
        int fatherMonthBorn=0;
        int fatherDayBorn=0;
        int fatherIq = 0;
        String fatherName ="";
        String fatherSurName ="";

        try {
          System.out.println("Введите имя Отца");
          fatherName = scanner.nextLine().trim();
        }catch (RuntimeException e){
          System.err.println("Вы ввели не корректное число");
          scanner.next();
        }
        try {
          System.out.println("Введите фамилию Отца");
          fatherSurName = scanner.nextLine().trim();
        }catch (RuntimeException e){
          System.err.println("Вы ввели не корректное число");
          scanner.next();
        }


        while (true){
          System.out.println("Введите год рождения Отца");
          try {
            fatherYearBorn = scanner.nextInt();
            if (fatherYearBorn<1900 || fatherYearBorn> 2050){
              continue;
            }
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
        while (true){
          System.out.println("Введите месяц рождения Отца");
          try {
            fatherMonthBorn = scanner.nextInt();
            if (fatherMonthBorn<0 || fatherMonthBorn> 12){
              continue;
            }
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
        while (true){
          System.out.println("Введите день рождения Отца");
          try {
            fatherDayBorn = scanner.nextInt();
            if (fatherDayBorn<0 || fatherDayBorn> 31){
              continue;
            }
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
        while (true){
          System.out.println("Введите IQ Отца");
          try {
            fatherIq = scanner.nextInt();
            if (fatherIq<0 || fatherIq> 220){
              continue;
            }
            scanner.nextLine();
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
       String motherDate = String.format("%02d/" + "%02d/" + "%d", motherDayBorn , motherMonthBorn, motherYearBorn);
       String fatherDate = String.format("%02d/" + "%02d/" + "%d", fatherDayBorn , fatherMonthBorn, fatherYearBorn);

       familyController.saveFamily(new Family(new Human(motherName, motherSurName, motherDate, motherIq), new Human(fatherName, fatherSurName, fatherDate, fatherIq)));
        System.out.println("Семья создана!");
      }
      ///////////////////////

      if (userInput == 7){
        int userIndex;
        while (true){
          System.out.println("Введите Индекс удаляемой семьи");
          try {
            userIndex = scanner.nextInt();
            if (userIndex<0 || userIndex> familyController.getAllFamilies().size()){
              continue;
            }
            scanner.nextLine();
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
        System.out.println("Удалено");
        familyController.deleteFamily(userIndex - 1);
      }
    ///
      if (userInput == 8){

        while (true){
          System.out.println("Редактирование семьи");
          System.out.println("1. Родить ребенка");
          System.out.println("2. Усыновить ребенка");
          System.out.println("3. Вернуться в главное меню");
          try {
            int subMenuNumber = scanner.nextInt();
              if(subMenuNumber < 0 || subMenuNumber > 3){
                subMenuNumber = scanner.nextInt();
              }
              if(subMenuNumber == 1){
                System.out.println("Введите номер семьи");
                int familyIndex = scanner.nextInt();
                scanner.nextLine();
                System.out.println("Введите имя для мальчика");
                String boyName = scanner.nextLine();
                System.out.println("Введите имя для девочки");
                String girlName = scanner.nextLine();

                familyController.bornChild((familyController.getFamilyByIndex(familyIndex -1)), boyName, girlName);
              }

            if(subMenuNumber == 2){
              int childYearBorn = 0;
              int childMonthBorn = 0;
              int childDayBorn = 0;
              int childIq = 0;

              System.out.println("Введите номер семьи");
              int familyIndex = scanner.nextInt();
              scanner.nextLine();

              System.out.println("Введите имя ребенка");
              String childName = scanner.nextLine();

              while (true){
                System.out.println("Введите год рождения Ребенка");
                try {
                  childYearBorn = scanner.nextInt();
                  if (childYearBorn<1900 || childYearBorn> 2050){
                    continue;
                  }
                  break;
                }catch (RuntimeException e){
                  System.err.println("Вы ввели не корректное число");
                  scanner.next();
                }
              }
              while (true){
                System.out.println("Введите месяц рождения Ребенка");
                try {
                  childMonthBorn = scanner.nextInt();
                  if (childMonthBorn<0 || childMonthBorn> 12){
                    continue;
                  }
                  break;
                }catch (RuntimeException e){
                  System.err.println("Вы ввели не корректное число");
                  scanner.next();
                }
              }
              while (true){
                System.out.println("Введите день рождения Ребенка");
                try {
                  childDayBorn = scanner.nextInt();
                  if (childDayBorn<0 || childDayBorn> 31){
                    continue;
                  }
                  break;
                }catch (RuntimeException e){
                  System.err.println("Вы ввели не корректное число");
                  scanner.next();
                }
              }
              while (true){
                System.out.println("Введите IQ Ребенка");
                try {
                  childIq = scanner.nextInt();
                  if (childIq<0 || childIq> 220){
                    continue;
                  }
                  scanner.nextLine();
                  break;
                }catch (RuntimeException e){
                  System.err.println("Вы ввели не корректное число");
                  scanner.next();
                }
              }
              String childDate = String.format("%02d/" + "%02d/" + "%d", childDayBorn , childMonthBorn, childYearBorn);
              familyController.adoptChild((familyController.getFamilyByIndex(familyIndex -1)), new Human(childName, "", childDate, childIq));

            }
            if(subMenuNumber == 3){
              break;
            }

          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
      }
      ///


      if (userInput == 9){
        int childrenAge;
        while (true){
          System.out.println("Введите возраст после которого дети будут удалены");
          try {
            childrenAge = scanner.nextInt();
            if (childrenAge<0 || childrenAge> 100){
              continue;
            }
            scanner.nextLine();
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }
        System.out.println("Удалено");
        familyController.deleteAllChildrenOlderThen(childrenAge);
      }
///
      if (userInput == 10){
        int familyIndex;
        while (true){
          System.out.println("Какую семью вывести на экран");
          try {
            familyIndex = scanner.nextInt();
            if (familyIndex<0 || familyIndex> familyController.getAllFamilies().size()){
              continue;
            }
            scanner.nextLine();
            break;
          }catch (RuntimeException e){
            System.err.println("Вы ввели не корректное число");
            scanner.next();
          }
        }

        familyController.printFamily(familyIndex-1);
      }
      if (userInput == 11){
        System.err.println("<Благодарю за визит! Успехов>");
        break;
      }


      ///
    }

  }
}

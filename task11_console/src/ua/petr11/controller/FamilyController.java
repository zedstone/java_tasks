package ua.petr11.controller;

import ua.petr11.Family;
import ua.petr11.Human;
import ua.petr11.Pet;
import ua.petr11.service.FamilyService;
import ua.petr11.service.FamilyServiceInt;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class FamilyController {
  private FamilyServiceInt familyService = new FamilyService();

  public List<Family> getFamiliesBiggerThan(int num) {
    return familyService.getFamiliesBiggerThan(num);
  }
  public List<Family> getFamiliesLessThan(int num) {
    return familyService.getFamiliesLessThan(num);
  }

  public int countFamiliesWithMemberNumber(int num) {
    return familyService.countFamiliesWithMemberNumber(num);
  }

  public void deleteAllChildrenOlderThen(int age) {
    familyService.deleteAllChildrenOlderThen(age);
  }

  public Set<Pet> getPets(int index) {
    return familyService.getPets(index);
  }

  public void addPet(int index, Pet pet) {
    familyService.addPet(index, pet);
  }

  public Family adoptChild(Family family, Human adoptChild){
    try {
      if(family.getFamilyLength() >= FamilyOverflowException.size){
        throw new FamilyOverflowException();
      }
      return familyService.adoptChild(family, adoptChild);
    }catch (FamilyOverflowException e){
      System.err.println(e.toString());
      return null;
    }

  }

  public Family bornChild(Family family, String boyName, String girlName){
    try {
      if(family.getFamilyLength() >= FamilyOverflowException.size){
        throw new FamilyOverflowException();
      }
      Random random = new Random();

      if(random.nextInt(11) <= 5) {
        System.out.println("Its a boy!");
        family.addChild(new Human(boyName, family.getFather().getSurname(), LocalDate.now()));
        saveFamily(family);
        return family;
      }else {
        family.addChild(new Human(girlName, family.getFather().getSurname(), LocalDate.now()));
        System.out.println("Its a girl!");
         saveFamily(family);
        return family;
    }

    }catch (FamilyOverflowException e){
      System.err.println(e.toString());
      return null;
    }

//    Random random = new Random();
//    if(random.nextInt(11) <= 5){
//      System.out.println("Its a boy!");
//      family.addChild(new Human(boyName, family.getFather().getSurname(), LocalDate.now()));
//      saveFamily(family);
//
//      return family;
//    }else {
//      family.addChild(new Human(girlName, family.getFather().getSurname(), LocalDate.now()));
//      System.out.println("Its a girl!");
//      saveFamily(family);
//
//      return family;
//    }

  }

  public int count(){
    return familyService.getAllFamilies().size();
  }

  public void createNewFamily(Human mother, Human father){
    familyService.saveFamily(new Family(mother, father));
  }

  public void displayAllFamilies(){
    List<Family> familyList = new ArrayList<>(familyService.getAllFamilies());
    for (int i = 0; i < familyList.size(); i++) {
      System.out.println(i + 1 +  ". " + familyList.get(i));
    }
    System.out.println("");

  }

  public List<Family> getAllFamilies() {
    return familyService.getAllFamilies();
  }


  public Family getFamilyByIndex(int index) {
    return familyService.getFamilyByIndex(index);
  }

  public void printFamily(int index) {
   familyService.getFamilyByIndex(index).prettyFormat();
  }


  public boolean deleteFamily(int index) {
    return familyService.deleteFamily(index);
  }


  public boolean deleteFamily(Family family) {
    return familyService.deleteFamily(family);
  }

  public void saveFamily(Family family) {
    familyService.saveFamily(family);
  }



}

class FamilyOverflowException extends RuntimeException{
  public static int size = 4;

  public String toString(){
    return "Семья переполнина, в ней не может быть больше " + size + " человек";
  }
}

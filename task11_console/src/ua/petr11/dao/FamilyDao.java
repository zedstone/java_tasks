package ua.petr11.dao;

import ua.petr11.Family;

import java.util.List;

public interface FamilyDao {
  List<Family> getAllFamilies();

  Family getFamilyByIndex(int index);

  boolean deleteFamily(int index);

  boolean deleteFamily(Family family);

  void saveFamily(Family family);


}

package ua.petr11;

public enum Days {
      MONDAY,
      TUESDAY,
      WEDNESDAY,
      THURSDAY,
      FRIDAY,
      SATURDAY,
      SUNDAY,
      REST,
      WORK,
      POOL,
      DATE,
      BEACH,
      PARK;
}
